FROM selenium/standalone-chrome:120.0
USER root
ENV DEBIAN_FRONTEND=noninteractive

RUN curl -s https://repos.azul.com/azul-repo.key | gpg --dearmor -o /usr/share/keyrings/azul.gpg  &&\
    echo "deb [signed-by=/usr/share/keyrings/azul.gpg] https://repos.azul.com/zulu/deb stable main" | tee /etc/apt/sources.list.d/zulu.list && \
    apt-get update &&\
    apt-get install -y zulu17-jdk-headless &&\
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*    
